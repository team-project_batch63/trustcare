import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnInit {
  @Output() captchaValidated = new EventEmitter<boolean>();

  captchaText: string = '';
  userInput: string = '';
  validationMessage: string = '';
 
  ngOnInit(): void {
    this.generateCaptcha();
  }

  generateCaptcha(): void {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < 6; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    this.captchaText = result;
  }

  validateCaptcha(): void {
    if (this.userInput === this.captchaText) {
      this.validationMessage = 'CAPTCHA validated successfully!';
      this.captchaValidated.emit(true);
    } else {
      this.validationMessage = 'Invalid CAPTCHA. Please refresh it.';
      this.captchaValidated.emit(false);
    }
  }

  refreshCaptcha(): void {
    this.generateCaptcha();
    this.userInput = '';
    this.validationMessage = '';
  }
}
