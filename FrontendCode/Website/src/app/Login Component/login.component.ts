import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  customers: any = {};
  email: any;
  Password: any;
  cust: any;
  captchaVerified: boolean = false;

  constructor(private router: Router, private service: RegisterService) {
    this.email = 'admin@gmail.com';
    this.Password = 'admin@123';
  }

  ngOnInit(): void {
    this.service.getAllCustomers().subscribe((item: any) => {
      this.cust = item;
    });
  }

  onCaptchaValidated(valid: boolean): void {
    this.captchaVerified = valid;
  }

  loginSubmit(loginForm: any): void {
    console.log(loginForm); // Check form values
    
    if (!this.captchaVerified) {
      alert("Please verify the captcha.");
      return;
    }
    
    if (loginForm.email === 'admin@gmail.com' && loginForm.password === 'admin@123') {
      alert("Admin login successful");
      this.service.setUserLoggedIn();
      this.router.navigate(['header-admin']);
      return;
    }

    if (!this.cust) {
      console.error("Customer data not available");
      return;
    }

    const matchedUser = this.cust.find((user: any) => {
      return user.email === loginForm.email && user.Password === loginForm.password;
    });

    if (matchedUser) {
      alert("User login successful");
      this.service.setUserLoggedIn();
      this.router.navigate(['/']);
    } else {
      alert("Invalid credentials, please try again");
    }
  }
}
