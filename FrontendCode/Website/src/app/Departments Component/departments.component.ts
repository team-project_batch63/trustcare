import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorsService } from '../doctors.service';
@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrl: './departments.component.css'
})
export class DepartmentsComponent {
  doctors: any[] = [];

  constructor(private router: Router, private doctorService: DoctorsService) { }

 

  onSpecialityChange(deptid: any): void {
    this.doctorService.getDoctorsByDepartment(deptid).subscribe((data: any[]) => {
      this.doctors = data;
    });
  }

  
}
