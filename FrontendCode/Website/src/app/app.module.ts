import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { DepartmentsComponent } from './departments/departments.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { HttpClientModule } from '@angular/common/http';
import { BookingComponent } from './booking/booking.component';
import { MatInputModule } from '@angular/material/input';
import { BlogComponent } from './blog/blog.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { SymptomsComponent } from './symptoms/symptoms.component';
import { CancerComponent } from './cancer/cancer.component';
import { TuberculosisComponent } from './tuberculosis/tuberculosis.component';
import { CovieshieldComponent } from './covieshield/covieshield.component';
import { HeartblogComponent } from './heartblog/heartblog.component';
import { LogoutComponent } from './logout/logout.component';
import { DentalComponent } from './dental/dental.component';
import { OncologistComponent } from './oncologist/oncologist.component';
import { CardiologistComponent } from './cardiologist/cardiologist.component';
import { NeurologistComponent } from './neurologist/neurologist.component';
import { OrthoComponent } from './ortho/ortho.component';
import { PediatricianComponent } from './pediatrician/pediatrician.component';
import { GynecologistComponent } from './gynecologist/gynecologist.component';
import { GastroentrologistComponent } from './gastroentrologist/gastroentrologist.component';
import { DermatologistComponent} from './dermatologist/dermatologist.component';
import { NutritionistComponent} from './nutritionist/nutritionist.component';
import { GeneralComponent} from './general/general.component';

import { NgxCaptchaModule } from 'ngx-captcha';
import { CaptchaComponent } from './captcha/captcha.component';
import { EmailComponent } from './email/email.component'; // Import NgxCaptchaModule

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    FooterComponent,
    ContactComponent,
    DepartmentsComponent,
    LoginComponent,
    RegisterComponent,
    LoginpageComponent,
    BookingComponent,
    BlogComponent,
    ForgotpasswordComponent,
    DoctorsComponent,
    SymptomsComponent,
    CancerComponent,
    TuberculosisComponent,
    CovieshieldComponent,
    HeartblogComponent,
    LogoutComponent,
    DentalComponent,
    OncologistComponent,
    CardiologistComponent,
    NeurologistComponent,
    OrthoComponent,
    PediatricianComponent,
    GynecologistComponent,
    GastroentrologistComponent,
    DermatologistComponent,
    NutritionistComponent,
    GeneralComponent,
    CaptchaComponent,
    EmailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    MatInputModule,
    NgxCaptchaModule // Add NgxCaptchaModule to imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
