import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  fullText: string = "Bringing Healthcare Closer with Us...";
  displayedChars: string = '';
  charIndex: number = 0;
  isDropdownOpen = false;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.showNextChar();
    // Listen for navigation start event to close the dropdown
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.isDropdownOpen = false;
      }
    });
  }

  showNextChar(): void {
    if (this.charIndex < this.fullText.length) {
      this.displayedChars += this.fullText[this.charIndex];
      this.charIndex++;
      setTimeout(() => this.showNextChar(), 70); // Adjust the speed as needed
    }
  }

  toggleDropdown(): void {
    this.isDropdownOpen = !this.isDropdownOpen;
  }

  navigateToLogout(): void {
    this.isDropdownOpen = false;
    this.router.navigate(['/logout']);
  }
}
