import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  private baseUrl = 'http://localhost:8083';

  constructor(private http: HttpClient) { }

  getDoctorsByDepartment(deptid: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/getAllDoctorsBydeptid/${deptid}`);
  }
}
