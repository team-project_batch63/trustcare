import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { BlogService } from  '../blog.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrl: './blog.component.css'
})
export class BlogComponent implements OnInit{
  blogs: any[] = [];
  selectedBlog: any = null; // To keep track of the selected blog

  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
    this.fetchAllBlogs();
  }

  fetchAllBlogs(): void {
    this.blogService.getAllBlogs().subscribe(
      (data: any[]) => {
        this.blogs = data;
      },
      (error) => {
        console.error('Error fetching blogs:', error);
      }
    );
  }

  showBlogDetails(blog: any): void {
    this.selectedBlog = blog;
  }

  backToList(): void {
    this.selectedBlog = null;
  }
}
