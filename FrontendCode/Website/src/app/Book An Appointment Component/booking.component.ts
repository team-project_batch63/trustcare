import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookingService } from '../booking.service'; // Import your BookingService

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent {
  appointmentData = {
    name: '',
    gender: '',
    mobile: '',
    email: '',
    department: '',
    date: '',
    time: ''
  };

  agreedToTerms = false;
  todayDate: string;
  minDate: string;
  maxDate: string;

  @ViewChild('bookingForm') bookingForm!: NgForm;

  constructor(private bookingService: BookingService) {
    this.todayDate = this.formatDate(new Date());
    this.minDate = this.todayDate;
    this.maxDate = '9999-12-31';
  }

  formatDate(date: Date): string {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  generateOTP() {
    console.log('Generating OTP...');

    // Call your booking service to save data in MySQL
    this.bookingService.createBooking(this.appointmentData).subscribe(
      (response) => {
        console.log('Booking created successfully:', response);

        // Reset the form after successful submission
        this.appointmentData = {
          name: '',
          gender: '',
          mobile: '',
          email: '',
          department: '',
          date: '',
          time: ''
        };
        this.agreedToTerms = false;

        // Use ViewChild to reset the form
        if (this.bookingForm) {
          this.bookingForm.resetForm();
        }

        // Display success message
        alert('Booking submitted successfully.');
      },
      (error) => {
        console.error('Error creating booking:', error);
        alert('Failed to submit booking. Please try again.');
      }
    );
  }
}
