import { Component } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dermatologist',
  templateUrl: './dermatologist.component.html',
  styleUrl: './dermatologist.component.css'
})
export class DermatologistComponent {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(9).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }

}
