import { Component } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurologist',
  templateUrl: './neurologist.component.html',
  styleUrl: './neurologist.component.css'
})
export class NeurologistComponent {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(4).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }
}
