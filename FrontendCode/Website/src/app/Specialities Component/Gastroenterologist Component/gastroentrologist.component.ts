import { Component } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gastroentrologist',
  templateUrl: './gastroentrologist.component.html',
  styleUrl: './gastroentrologist.component.css'
})
export class GastroentrologistComponent {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(8).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }
}
