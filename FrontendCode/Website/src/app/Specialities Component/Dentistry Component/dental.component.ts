import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { DoctorsService } from '../doctors.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dental',
  templateUrl: './dental.component.html',
  styleUrl: './dental.component.css'
})
export class DentalComponent implements OnInit {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(1).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }
}
