import { Component } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gynecologist',
  templateUrl: './gynecologist.component.html',
  styleUrl: './gynecologist.component.css'
})
export class GynecologistComponent {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(7).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }
}
