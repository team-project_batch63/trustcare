import { Component } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ortho',
  templateUrl: './ortho.component.html',
  styleUrl: './ortho.component.css'
})
export class OrthoComponent {
  DoctorsList: any[] = [];
  list: any[] = [];

  constructor(private service: DoctorsService, private serv: RegisterService, private router: Router) {}

  ngOnInit(): void {
    this.getAllDoctorsByDepartment();
  }

  getAllDoctorsByDepartment(): void {
    this.service.getDoctorsByDepartment(5).subscribe((res: any) => { 
      this.DoctorsList = res;
    });
  }
}
