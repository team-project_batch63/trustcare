import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private baseUrl = 'http://localhost:8083'; // Replace with your Spring Boot backend URL

  constructor(private http: HttpClient) { }

  // POST method to create a new booking
  createBooking(appointmentData: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/createbooking`, appointmentData);
  }

  // GET method to fetch all bookings
  getAllBookings(): Observable<any> {
    return this.http.get(`${this.baseUrl}/getallbookings`);
  }

  // GET method to fetch a booking by ID
  getBookingById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/getbookingbyid/${id}`);
  }

  // DELETE method to delete a booking by ID
  deleteBookingById(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/deletebookingbyid/${id}`);
  }
}
