import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterService } from '../register.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  fullText: string = "Bringing Healthcare Closer with Us...";
  chars: string[];
  displayedChars: string[] = [];
  charIndex: number = 0;
  errorMessage: string = '';

  constructor(private registerService: RegisterService) {
    this.chars = this.fullText.split('');
  }

  ngOnInit(): void {
    this.showNextChar();
  }

  showNextChar(): void {
    if (this.charIndex < this.chars.length) {
      this.displayedChars.push(this.chars[this.charIndex]);
      this.charIndex++;
      setTimeout(() => this.showNextChar(), 70); // Adjust the speed as needed
    }
  }

  faqs = [
    {
      question: 'How to consult Doctors Online Now?',
      answer: `Patient is required to follow the mentioned steps before contacting the doctor:<br>
      1. Go through available doctors & click Consult Now<br>
      2. Fill the consultation form (patient name, email (optional), mobile number)<br>
      3. Patient receives the OTP on the mentioned number<br>
      4. Patient Pays for the consultation<br>
      5.  TrustCare representative contacts the patient within 30 mins to gather more information<br>
     `,
      showAnswer: false
    },
    {
      question: 'What is the minimum fee for Online Doctor Consultation?',
      answer: 'Online doctor consultation at TrustCare starts at Rs.500. The consultation fee will vary based on the doctor, the specialty, years of experience of the doctor, any discounts or promotion applied, etc.',
      showAnswer: false
    },
    {
      question: 'Will I Get a Refund if I cancel the online Doctor Consultation? ',
      answer: 'Yes. If you cancel the online consultation 1hour prior to Scheduled Appointemnet, the refund for the same will reflect in your account within 5 to 7 working days.'
    },
    {
      question: ' How long do I have to wait for the consultation?',
      answer: 'TrustCare representative will contact you within 30 minutes and she will coordinate with you for your consultation time as per the doctor’s availability.The estimated time of waiting is less than 1 hour.'
    },
    {
      question: 'Are there any medications that TrustCare doctors cannot prescribe?',
      answer: 'There are some medications that are always prescribed in-person and cannot be prescribed by TrustCare doctors (or any other doctor seeing a patient online). These include opioids and other controlled substances and some psychiatric and lifestyle drugs.'
    }
  ];

  toggleAnswer(faq: any): void {
    faq.showAnswer = !faq.showAnswer;
  }

  submitForm(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    const contact = {
      firstname: form.value.firstname,
      email: form.value.email,
      country: form.value.country,
      address: form.value.address,
      message: form.value.message
    };

    this.registerService.saveContactForm(contact)
      .subscribe(
        (response: HttpResponse<any>) => {
          if (response.status === 200) {
            alert('Form submitted successfully!');
          } else if (response.status === 409) {
            alert('Form is already submitted, we will connect you shortly.');
          }
          form.resetForm();
          this.errorMessage = ''; // Clear any previous error message
        },
        (error: HttpErrorResponse) => {
          console.error('Error submitting contact form:', error);
          if (error.status === 409) {
            alert('Form is already submitted, we will connect you shortly.');
          } else if (error.status === 200) {
            alert('Form submitted successfully!');
          } else {
            alert('An unexpected error occurred.');
          }
        }
      );
  }
}
