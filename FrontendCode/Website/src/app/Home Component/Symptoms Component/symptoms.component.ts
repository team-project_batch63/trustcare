import { Component } from '@angular/core';

@Component({
  selector: 'app-symptoms',
  templateUrl: './symptoms.component.html',
  styleUrl: './symptoms.component.css'
})
export class SymptomsComponent {
  cards = [
    {
      image: 'https://www.practo.com/consult/static/images/cough-cold-v1.jpg',
      title: 'Cough & Cold'
    },
    {
      image: 'https://i-cf65.ch-static.com/content/dam/cf-consumer-healthcare/bp-advil-v2/en_US/pages/homepage/fever-in-adults-1786x1191.jpg?auto=format',
      title: 'Fever'
    },
    {
      image: 'https://khealth.com/wp-content/uploads/2021/07/chest-pain.jpg',
      title: 'Chest Pain'
    },
    {
      image: 'https://sa1s3optim.patientpop.com/assets/images/provider/photos/2498498.jpg',
      title: 'Headache'
    },
    {
      image: 'https://hje.org.uk/wp-content/uploads/2024/01/home-remedies-for-stomach-pain.jpg',
      title: 'Stomach Issues'
    },
    {
      image: 'https://t3.ftcdn.net/jpg/04/06/49/36/360_F_406493603_R9oDN5wDCDAHitbpyWwcu1RmlifG4XkM.jpg',
      title: 'Depression'
    },
    {
      image: 'https://s.yimg.com/ny/api/res/1.2/P2QPZbN.wt8Pgz6d5tGwwg--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MDtoPTQyNw--/https://media.zenfs.com/en/galvanized/e4227e313cca4e0cfc722cd66d5f70df',
      title: 'Sore Throat'
    },
    {
      image: 'https://www.practo.com/consult/static/images/sick-kid-v1.jpg',
      title: 'Sick Kid'
    },
    {
      image: 'https://www.kidney.org/sites/default/files/styles/newsletter_article_image__scale_width_to_630px_/public/itching_and_kidney_disease.png?itok=zFXz3Vf8',
      title: 'Skin Problems'
    }
  ];

  // Flag to control card display
  showAllCards = false;

  // Method to toggle card display
  toggleCardsDisplay() {
    this.showAllCards = !this.showAllCards;
  }
}
