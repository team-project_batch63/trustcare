import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DepartmentsComponent } from './departments/departments.component';
import { HomeComponent } from './home/home.component';
import { BookingComponent } from './booking/booking.component';
import { BlogComponent } from './blog/blog.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { CancerComponent } from './cancer/cancer.component';
import { HeartblogComponent } from './heartblog/heartblog.component';
import { TuberculosisComponent } from './tuberculosis/tuberculosis.component';
import { CovieshieldComponent } from './covieshield/covieshield.component';
import { LogoutComponent } from './logout/logout.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { DentalComponent } from './dental/dental.component';
import { NeurologistComponent } from './neurologist/neurologist.component';
import { OncologistComponent } from './oncologist/oncologist.component';
import { CardiologistComponent } from './cardiologist/cardiologist.component';
import { OrthoComponent } from './ortho/ortho.component';
import { PediatricianComponent } from './pediatrician/pediatrician.component';
import { GynecologistComponent } from './gynecologist/gynecologist.component';
import { GastroentrologistComponent } from './gastroentrologist/gastroentrologist.component';
import { GeneralComponent } from './general/general.component';
import { DermatologistComponent } from './dermatologist/dermatologist.component';
import { NutritionistComponent } from './nutritionist/nutritionist.component';

const routes: Routes = [
 {path:'' ,component:HomeComponent},
  {path:'register',    component:RegisterComponent},
  {path:'login',       component:LoginComponent},
  {path:'about',       component:AboutComponent},
  {path:'contact',     component:ContactComponent},
  {path:'departments', component:DepartmentsComponent},
  {path:'booking',  component:BookingComponent},
  {path:'blog',component:BlogComponent},
  {path:'forgot',component:ForgotpasswordComponent},
  {path:'read',component:CancerComponent},
  {path:'heartblog',component:HeartblogComponent},
  {path:'tuberblog',component:TuberculosisComponent},
  {path:'vaccine',component:CovieshieldComponent},
  {path:'logout',component:LogoutComponent},
  { path: 'doctors', component: DoctorsComponent },
  {path:'dental',component:DentalComponent},
  {path:'neuro',component:NeurologistComponent},
  {path:'oncologist',component:OncologistComponent},
  {path:'cardiologist',component:CardiologistComponent},
  {path:'ortho',component:OrthoComponent},
  {path:'children',component:PediatricianComponent},
  {path:'gynecologist',component:GynecologistComponent},
  {path:'gastro',component:GastroentrologistComponent},
  {path:'general',component:GeneralComponent},
  {path:'dermal',component:DermatologistComponent},
  {path:'nutrition',component:NutritionistComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
