import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  contact = {
    firstname: '',
    email: '',
    country: '',
    address: '',
    message: ''
  };

  errorMessage: string = '';

  constructor(private registerService: RegisterService) {}

  submitForm(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    this.registerService.saveContactForm(this.contact)
      .subscribe(
        () => {
          this.errorMessage = ''; // Clear any previous error message
          alert('Form submitted successfully!');
          form.resetForm();
        },
        (error: HttpErrorResponse) => {
          console.error('Error submitting contact form:', error);
          if (error.status === 200) {
            alert('Form submitted successfully!');
          }else {
            alert('Form Is Already Submitted, We will Connect You Shortly ');
          }
        }
      );
  }
}
