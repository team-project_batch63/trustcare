import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  usersData = {
    Password: ''
};
hidePassword = true;

togglePasswordVisibility() {
    this.hidePassword = !this.hidePassword;
}

  userData: any = {};

  constructor(private router: Router, private registerService: RegisterService) {}

  userpatient(registerForm: any): void {
    if (registerForm.valid) {
      this.registerService.register(this.userData).subscribe(
        (response) => {
          alert("Registration Successful");
          console.log('Registration successful:', response);
          this.router.navigate(['/login']);
        },
        (error) => {
          alert("Registration Failed");
          console.error('Registration failed:', error);
        }
      );
    } else {
      alert("Please fill in all required fields correctly.");
    }
    
  }
}
