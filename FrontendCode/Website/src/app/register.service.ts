import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  isUserloggedIn :any; 
  loginStatus: Subject<any>;      
  private baseUrl = 'http://localhost:8083'; // Adjust the base URL as needed
 constructor(private http: HttpClient, private router: Router) {
   this.isUserloggedIn=true;
   this.loginStatus = new Subject();
  }

  getLoginStatus(): any {
   return this.loginStatus.asObservable();
 }

 setUserLoggedIn(): any {
   this.isUserloggedIn = true;
   this.loginStatus.next(true);   
 }

 getUserLoggedStatus(): boolean {
   return this.isUserloggedIn;
 }

 setUserLogout(): void {
   this.isUserloggedIn = false;
   this.loginStatus.next(false);
 }

 logout(): void {
   // Clear the user session or token
   localStorage.removeItem('token');
 
   // Set the user login status to false
   this.isUserloggedIn = false;
 
   // Redirect to the login page
   this.router.navigate(['/header/login']);
 }

 register(data: any) {
   return this.http.post("http://localhost:8083/registeruser", data);
 }

 getAllCustomers() {
   return this.http.get("http://localhost:8083/getallnames");
 }
 login(email: string, Password: string){
  return this.http.get("http://localhost:8083/loginUser/${email}/${Password}");
 }
 saveContactForm(formData: any) {
  // Adjust the API endpoint URL according to your backend setup
  return this.http.post<any>('http://localhost:8083/contactform', formData);
}
 


}
