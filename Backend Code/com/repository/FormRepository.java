package com.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.model.Form;

@Repository
public interface FormRepository extends JpaRepository<Form, Integer> {
	@Query("SELECT u FROM Form u WHERE u.email= :email AND u.Password = :Password")
	Optional<Form> findByUserNamePassword(String email, String Password);
	 Optional<Form> findByEmail(String email);
	
	
}
