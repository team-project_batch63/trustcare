package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Doctors;
@Repository
public interface DoctorsRepository extends JpaRepository<Doctors,Integer>{

	List<Doctors> findByDepartment(String dept);
	@Query("SELECT d FROM Doctors d WHERE d.department.deptid = :deptid")
	List<Doctors> findAllByDepartmentId(@Param("deptid")int deptid);
}