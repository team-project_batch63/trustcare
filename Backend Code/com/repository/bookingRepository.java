package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Booking;
@Repository
public interface bookingRepository extends JpaRepository<Booking, Integer>{

}
