package com.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Department {
    @OneToMany(mappedBy = "department")
    @JsonIgnore
    private List<Doctors> docList = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int deptid;

    private String deptname;

    // Getters and Setters
    public List<Doctors> getDocList() {
        return docList;
    }

    public void setDocList(List<Doctors> docList) {
        this.docList = docList;
    }

    public int getDeptid() {
        return deptid;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    @Override
    public String toString() {
        return "Department [docList=" + docList + ", deptid=" + deptid + ", deptname=" + deptname + "]";
    }
}
