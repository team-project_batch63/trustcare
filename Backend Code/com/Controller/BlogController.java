package com.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BlogDao;
import com.model.Blog;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class BlogController {
	@Autowired
	BlogDao blogdao;
	@RequestMapping("getallblogs")
	public List<Blog>getallblogs(){
		return blogdao.getallblogs();
	}
	@PostMapping("createblog")
public void blog(@RequestBody Blog data) {
		 blogdao.createBlog(data);
	}
	  @GetMapping("/getblogbyid/{id}")
	    public Optional<Blog> getBlogById(@PathVariable("id") int id) {
	        return blogdao.getBlogById(id);
	    }
	  @DeleteMapping("deleteblogbyid/{id}")
		public String deleteblog(@PathVariable("id") int id) {
			blogdao.deleteblog(id);
			return "Product Deleted Successfully!!";
		}

	

	
	
}
