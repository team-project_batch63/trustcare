package com.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.bookingDao;

import com.model.Booking;
@CrossOrigin
(origins="http://localhost:4200")
@RestController
public class bookingController {
@Autowired
bookingDao bookdao;
@RequestMapping("getallbookings")
public List<Booking>getallbookings(){
	return bookdao.getallbookings();
}
@PostMapping("createbooking")
public void Booking(@RequestBody Booking booking) {
	 bookdao.createbooking(booking);
}
  @GetMapping("/getbookingbyid/{id}")
    public Optional<Booking> getBookingsById(@PathVariable("id") int id) {
        return bookdao.getBookingsById(id);
    }
  @DeleteMapping("deletebookingbyid/{id}")
	public String deletebooking(@PathVariable("id") int id) {
		bookdao.deletebooking(id);
		return "Product Deleted Successfully!!";
	}

}
