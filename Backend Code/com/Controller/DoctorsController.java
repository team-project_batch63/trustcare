package com.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DoctorsDao;
import com.model.Doctors;
@CrossOrigin
(origins="http://localhost:4200")
@RestController
public class DoctorsController {
	@Autowired
	DoctorsDao docdao;
	@GetMapping("getalldoctors")
	public List<Doctors>getAllDoctors(){
		return docdao.getAllDoctors();
	}
	@GetMapping("getdoctorsbyid/{id}")
		public Doctors getDoctorsById(@PathVariable("id") int id){
			return docdao.getDoctorsById(id);
		}
	@GetMapping("getdoctorsbydept/{dept}")
	public List<Doctors> getDoctorByDepartment(@PathVariable("dept")String dept){
		return docdao.getDoctorsByDepartment(dept);
	}
	@PostMapping("addDoctor")
	public Doctors addDoctor(@RequestBody Doctors doc){
		return docdao.addDoctor(doc);
		
	}
	@PutMapping("updatedoctor")
	public Doctors updatedoctor(@RequestBody Doctors doc){
		return docdao.updatedoctor(doc);
	}
	@DeleteMapping("deletealldoctor")
	public String deleteAllDoctors(){
		docdao.deleteAllDoctors();
		return "alldeleted";
		
	}
	@DeleteMapping("deletedoctorsbyid/{id}")
	public String deleteDoctorsById(@PathVariable("id") int id){
		docdao.deleteDoctorsById(id);
		return "deleted doctors by id";
	}
	@GetMapping("getAllDoctorsBydeptid/{deptid}")
	public List<Doctors> getAllDoctorsBydeptid(@PathVariable("deptid")int deptid){
		return docdao.getAllDoctorsBydeptid(deptid);
	}
	
		
	}
