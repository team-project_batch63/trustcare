package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDao;

import com.model.Department;
@CrossOrigin
(origins="http://localhost:4200")
@RestController
public class DepartmentController {
	@Autowired
	DepartmentDao deptdao;
	@GetMapping("getDepartmentById/{deptid}")
	public Department getDepartmentById(@PathVariable("deptid")int deptid){
		return deptdao.getDepartmentById(deptid);
	}
	@GetMapping("getalldepartments")
	public List<Department> getalldepartments(){
		return deptdao.getalldepartments();
	}
	@PostMapping("createdepartment")
	public void createDepartment(@RequestBody Department dept) {
		 deptdao.createDepartment(dept);
	}
}
