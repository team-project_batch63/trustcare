package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.Emailservice;
import com.model.Otp;
@RestController
public class Otpcontroller {
	@Autowired
    private Emailservice emailService;

	@PostMapping("/generateotp")
    public String generateOtp(@RequestBody Otp otp) {
        String generateOtp = emailService.generateOtp();
        emailService.sendOtpEmail(otp.getEmail(), generateOtp);
        return "OTP sent successfully to " + otp.getEmail();
    }
}
