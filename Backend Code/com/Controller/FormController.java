package com.controller;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.FormDao;
import com.model.Form;
@CrossOrigin(origins="http://localhost:4200")
@RestController
public class FormController {
@Autowired
FormDao fordao;
private static final Logger logger = Logger.getLogger(FormController.class.getName());
@RequestMapping("getallnames")
public List<Form>getallnames(Form user){
	return fordao.getallnames(user);
	}
@PostMapping("registeruser")
public void form(@RequestBody Form user) {
	fordao.registeruser(user);
}
//api for contact form//
@PostMapping("contactform")
public ResponseEntity<String> saveContactForm(@RequestBody Form contactform) {
    logger.info("Checking email: " + contactform.getEmail());
    Optional<Form> existingForm = fordao.findByEmail(contactform.getEmail());
    if (existingForm.isPresent()) {
        logger.info("Email already exists: " + contactform.getEmail());
        return new ResponseEntity<>("Email already exists", HttpStatus.CONFLICT);
    } else {
        fordao.savecontactform(contactform);
        logger.info("Contact form submitted successfully for email: " + contactform.getEmail());
        return new ResponseEntity<>("Contact form submitted successfully", HttpStatus.OK);
    }
}
@RequestMapping("loginUser/{email}/{Password}")
public ResponseEntity<Form> loginUser(@PathVariable("email") String email, @PathVariable("Password") String Password) {
    Optional<Form> user = fordao.findByUserNamePassword(email, Password);
    if (user.isPresent()) {
        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    } else {
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);  // Or HttpStatus.NOT_FOUND based on your preference
    }
    
}
@GetMapping("getallsubscribers")
public List<Form>getallsubscribers(Form subscribeform){
	return fordao.getallsubscribers(subscribeform);
	}

}

