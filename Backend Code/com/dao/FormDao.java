package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Form;


@Service
public class FormDao {
@Autowired
FormRepository forepo;
public List<Form>getallnames(Form user ){
	return forepo.findAll();
}
public void registeruser(Form user) {
	// TODO Auto-generated method stub
	forepo.save(user);
	
}
public Optional<Form> findByUserNamePassword(String email, String Password) {
	return forepo.findByUserNamePassword(email,Password);
	
}

public void savesubscribeform(Form subscribeform) {
	
	forepo.save(subscribeform);
}
public void savecontactform(Form contactform) {
	
	forepo.save(contactform);
	
}
public Optional<Form> findByEmail(String email) {
    return forepo.findByEmail(email);
}
public List<Form>getallsubscribers(Form subscribeform ){
	return forepo.findAll();
}



}
