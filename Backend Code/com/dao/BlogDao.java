package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Blog;

@Service
public class BlogDao {
	@Autowired
BlogRepository blogrepo;
	//methods for crud operations//
public List<Blog>getallblogs(){
	return blogrepo.findAll();
	
}
public Optional<Blog> getBlogById(int id) {
    return blogrepo.findById(id);
}
public Blog createBlog(Blog blog) {
	return blogrepo.save(blog);
}
public void deleteblog(int id) {
	 blogrepo.deleteById(id);
}
}
