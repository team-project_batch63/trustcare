package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Doctors;

@Service
public class DoctorsDao {
@Autowired
DoctorsRepository docrepo;
public List<Doctors>getAllDoctors(){
	return docrepo.findAll();
}
public Doctors getDoctorsById(int id){
	return docrepo.findById(id).orElse(new Doctors());
		
	}
public Doctors addDoctor(Doctors doc){
	return docrepo.save(doc);
}
public Doctors updatedoctor(Doctors doc){
	return docrepo.save(doc);
}
public void deleteDoctorsById(int id){
	docrepo.deleteById(id);
}
public void deleteAllDoctors(){
	docrepo.deleteAll();
}
public List<Doctors> getDoctorsByDepartment(String dept){
	return docrepo.findByDepartment(dept);
}
public List<Doctors> getAllDoctorsBydeptid(int deptid) {
    return docrepo.findAllByDepartmentId(deptid);
}

}