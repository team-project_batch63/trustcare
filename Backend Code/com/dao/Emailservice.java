package com.dao;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@Service
public class Emailservice {

    @Autowired
    JavaMailSender javaMailSender;

    public String generateOtp() {
        UUID uuid = UUID.randomUUID();
        String uuidString = uuid.toString().replace("-", "").toLowerCase();
        String otp = uuidString.substring(0, 6);
        return otp;
    }

    public void sendOtpEmail(String toEmail, String otp) {
        String subject = "Your OTP Code";
        String body = "Thank you for choosing TrustCare! We've sent an OTP to your email to verify your appointment booking.\n\n"
                + "Your OTP code is: " + otp + "\n\n"
                + "Please enter OTP to Confirm Your Appointment";

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("TrustCare@gmail.com");
        mailMessage.setTo(toEmail);
        mailMessage.setSubject(subject);
        mailMessage.setText(body);

        javaMailSender.send(mailMessage);
    }

    // Other methods related to email verification, request, and registration
    public void sendRequestEmail(String toEmail, String userName, String requesterName, String requesterEmail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(toEmail);
        message.setSubject("Matrimony Request from Wedlock");
        message.setText("Hello " + userName + ",\n\nYou have received a new matrimony request from " 
                + requesterName + " (" + requesterEmail + ").\n\nBest regards,\nWedLock");
        javaMailSender.send(message);
    }

    public void registerEmailSender(String toEmail, String userName, String userId) {
        String subject = "Registered Successfully";
        String body = "Hi " + userName + "!\n\n"
                + "Thank you for registering !!\n\n"
                + "Your Id is " + userId + "\n\n\n\n\n\n\n"
                + "Best Regards,\n"
                + "The Wedlock Team";

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("TrustCare@gmail.com");
        mailMessage.setTo(toEmail);
        mailMessage.setSubject(subject);
        mailMessage.setText(body);

        javaMailSender.send(mailMessage);
    }
}
