package com.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.Department;

@Service
public class DepartmentDao {
@Autowired
DepartmentRepository deptrepo;
public List<Department>getalldepartments(){
	return deptrepo.findAll();
	
}

public Department createDepartment(Department dept) {
	return deptrepo.save(dept);
}
public Department getDepartmentById(int deptid) {
	// TODO Auto-generated method stub
	return deptrepo.findById(deptid).orElse(new Department());
}

}


