package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Blog;
import com.model.Booking;

@Service
public class bookingDao {
@Autowired
bookingRepository bookrepo;
public List<Booking>getallbookings(){
	return bookrepo.findAll();
	
}
public Optional<Booking> getBookingsById(int id) {
    return bookrepo.findById(id);
}
public Booking createbooking(Booking booking) {
	return bookrepo.save(booking);
}
public void deletebooking(int id) {
	 bookrepo.deleteById(id);
}
}
